from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required

# Create your views here.
# @login_required(login_url='/story9/login/')
def view_story9(request):
    return render(request, "home.html")

def view_login(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('/story9')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})

def view_logout(request):
    logout(request)
    return redirect('/story9')
